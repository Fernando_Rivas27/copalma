from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

#django.contrib.auth.models.User
# Create your views here.
"""pagina estatica"""
@login_required
def home(request):
    return render(request, "core/home.html")
"""pagina estatica"""
@login_required
def about(request):
    return render(request, "core/about.html")


"""pagina estatica"""
@login_required
def store(request):
    return render(request, "core/store.html")
"""pagina estatica"""



